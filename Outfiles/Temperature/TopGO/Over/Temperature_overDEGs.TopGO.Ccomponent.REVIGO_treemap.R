

# A treemap R script produced by the REVIGO server at http://revigo.irb.hr/
# If you found REVIGO useful in your work, please cite the following reference:
# Supek F et al. "REVIGO summarizes and visualizes long lists of Gene Ontology
# terms" PLoS ONE 2011. doi:10.1371/journal.pone.0021800

# author: Anton Kratz <anton.kratz@gmail.com>, RIKEN Omics Science Center, Functional Genomics Technology Team, Japan
# created: Fri, Nov 02, 2012  7:25:52 PM
# last change: Fri, Nov 09, 2012  3:20:01 PM

# -----------------------------------------------------------------------------
# If you don't have the treemap package installed, uncomment the following line:
# install.packages( "treemap" );
library(treemap) 								# treemap package by Martijn Tennekes

# Set the working directory if necessary
# setwd("C:/Users/username/workingdir");

# --------------------------------------------------------------------------
# Here is your data from REVIGO. Scroll down for plot configuration options.

revigo.names <- c("term_ID","description","freqInDbPercent","value","uniqueness","dispensability","representative");
revigo.data <- rbind(c("GO:0005576","extracellular region",2.375,32.0000,0.888,0.000,"extracellular region"),
c("GO:0005581","collagen trimer",0.071,7.0000,0.854,0.000,"collagen trimer"),
c("GO:0005615","extracellular space",0.538,12.0000,0.803,0.000,"extracellular space"),
c("GO:0005788","endoplasmic reticulum lumen",0.027,14.0000,0.611,0.000,"endoplasmic reticulum lumen"),
c("GO:0033018","sarcoplasmic reticulum lumen",0.001,4.0000,0.647,0.481,"endoplasmic reticulum lumen"),
c("GO:0005737","cytoplasm",26.017,67.0000,0.774,0.139,"endoplasmic reticulum lumen"),
c("GO:0034663","endoplasmic reticulum chaperone complex",0.003,2.0000,0.610,0.506,"endoplasmic reticulum lumen"),
c("GO:0000324","fungal-type vacuole",0.110,1.0000,0.691,0.269,"endoplasmic reticulum lumen"),
c("GO:0001750","photoreceptor outer segment",0.018,2.0000,0.738,0.254,"endoplasmic reticulum lumen"),
c("GO:0048471","perinuclear region of cytoplasm",0.135,7.0000,0.701,0.243,"endoplasmic reticulum lumen"),
c("GO:0042470","melanosome",0.016,9.0000,0.710,0.212,"endoplasmic reticulum lumen"),
c("GO:0005790","smooth endoplasmic reticulum",0.005,2.0000,0.649,0.468,"endoplasmic reticulum lumen"),
c("GO:0009986","cell surface",0.241,11.0000,0.825,0.043,"cell surface"),
c("GO:0009897","external side of plasma membrane",0.064,6.0000,0.778,0.046,"external side of plasma membrane"),
c("GO:0046658","anchored component of plasma membrane",0.030,2.0000,0.781,0.470,"external side of plasma membrane"),
c("GO:0042597","periplasmic space",0.671,2.0000,0.820,0.056,"periplasmic space"),
c("GO:0016021","integral component of membrane",55.868,25.0000,0.935,0.060,"integral component of membrane"));

stuff <- data.frame(revigo.data);
names(stuff) <- revigo.names;

stuff$value <- as.numeric( as.character(stuff$value) );
stuff$freqInDbPercent <- as.numeric( as.character(stuff$freqInDbPercent) );
stuff$uniqueness <- as.numeric( as.character(stuff$uniqueness) );
stuff$dispensability <- as.numeric( as.character(stuff$dispensability) );

# by default, outputs to a PDF file
pdf( file="revigo_treemap.pdf", width=16, height=9 ) # width and height are in inches

# check the tmPlot command documentation for all possible parameters - there are a lot more
tmPlot(
	stuff,
	index = c("representative","description"),
	vSize = "value",
	type = "categorical",
	vColor = "representative",
	title = "REVIGO Gene Ontology treemap",
	inflate.labels = FALSE,      # set this to TRUE for space-filling group labels - good for posters
	lowerbound.cex.labels = 0,   # try to draw as many labels as possible (still, some small squares may not get a label)
	bg.labels = "#CCCCCCAA",     # define background color of group labels
												       # "#CCCCCC00" is fully transparent, "#CCCCCCAA" is semi-transparent grey, NA is opaque
	position.legend = "none"
)

dev.off()
