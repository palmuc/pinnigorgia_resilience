### Determination of calcification hot-spots along the body axis of *P. flava*

We used calcein, a calcium-binding fluorescent dye which is permanently incorporated into the skeletal tissue, to investigate the distribution of calcification sites along *P. flava*’s body axis (1).

We incubated three colonies of *P. flava* for 72 hours in a glass container with 500ml of a 50µg/ml calcein disodium salt (Sigma-Aldrich) in 0.2 µm filtered artificial seawater (see photo below). We exchanged the seawater with fresh seawater+calcein every 24 hours.


![Calcein staining example](./WP_20170116_16_22_57_Pro.jpg)


After staining, we fixed the colonies in 80% EtOH and stored them at ~5 °C until further processing. To assess whether calcification preferentially occurs on the tip of the colonies or, on the contrary, the calcification hot-spots are distributed along the colony body axis, we cut the colonies from top to bottom every 5 mm using a sterile scalpel, and placed each piece in 1.5 ml microcentrifuge tubes containing 1 ml of sodium hypochlorite (NaOCl 10%; Fluka). After three hours of incubation in bleach, we rinsed the sedimented sclerites six times with distilled water and stored them in 80% ethanol. To determine the number of stained sclerites per colony region, we placed a sample of sclerites onto glass slides, let the ethanol evaporate, embedded them in Eukitt quick-hardening mounting medium (Fluka Analytical) and covered the sample with a glass coverslip to dry for at least 24 hours. We then observed the sclerites under epifluorescence (excitation filter band pass 420-490 nm, barrier filter 515 nm long pass) on a Leica DMLB microscope coupled to a Leica DFC 480 camera and an I3 filter set. We exposed the stained sclerites for 10 seconds and acquired pictures using Leica Application Software “LAS V4.5”. For each tip, middle and bottom sclerite sample of every colony, we sampled along one horizontal transect crossing the slide from left to right and counted stained and total sclerites at a 100X magnification.

1. 	M. Holcomb, A. L. Cohen, D. C. McCorkle, An evaluation of staining techniques for marking daily growth in scleractinian corals. J. Exp. Mar. Bio. Ecol. 440, 126–131 (2013).