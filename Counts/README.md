### Analysis of differential gene expression

We extracted total RNA from the upper section of the octocorals using the Direct-zol RNA MiniPrep kit (Zymo Research) following the manufacturer’s protocol.

We assessed the purity and integrity of the RNA extracts using a Nanodrop ND-1000 spectrophotometer (Thermo Fisher Scientific, USA) and a Bioanalyzer 2100 (Agilent Inc., USA), and used Lexogen’s SENSE Total RNA-Seq Library Prep kit according to the manufacturer’s instructions to generate Illumina-ready transcriptomic libraries for all samples.

We sequenced the libraries (50 PE) in a HiSeq2000, quality controlled them with the program filter-illumina from the BioLite suite (1) and mapped them to an available *P. flava* reference transcriptome (2) using Salmon (3). We analysed the resulting count matrix using DESeq2 (4) and used the resulting list of differentially expressed genes (Benjamini-Hochberg p < 0.05 and Log2 Fold Change ≥ |1|) for GO-term enrichment analyses using TopGO (5), REVIGO (6), and CirGO (7).


1. 	M. Howison, N. A. Sinnott-Armstrong, C. W. Dunn, BioLite, a Lightweight Bioinformatics Framework with Automated Tracking of Diagnostics and Provenance in TaPP, (2012).
2. 	N. Conci, G. Wörheide, S. Vargas, New Non-Bilaterian Transcriptomes Provide Novel Insights into the Evolution of Coral Skeletomes. Genome Biol. Evol. 11, 3068–3081 (2019).
3. 	R. Patro, G. Duggal, M. I. Love, R. A. Irizarry, C. Kingsford, Salmon provides fast and bias-aware quantification of transcript expression. Nat. Methods 14, 417–419 (2017).
4. 	M. I. Love, W. Huber, S. Anders, Moderated estimation of fold change and dispersion for RNA-seq data with DESeq2. Genome Biol. 15, 550 (2014).
5. 	A. Alexa, J. Rahnenfuhrer, topGO: Enrichment Analysis for Gene Ontology (2016).
6. 	F. Supek, M. Bošnjak, N. Škunca, T. Šmuc, REVIGO summarizes and visualizes long lists of gene ontology terms. PLoS One 6, e21800 (2011).
7. 	I. Kuznetsova, A. Lugmayr, S. J. Siira, O. Rackham, A. Filipovska, CirGO: an alternative circular way of visualising gene ontology terms. BMC Bioinformatics 20, 84 (2019).
