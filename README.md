## Resilience to climate-change in an octocoral involves the transcriptional decoupling of the calcification and stress response toolkits

Sergio Vargas<sup>1*</sup>, Thorsten Zimmer<sup>1</sup>, Nicola Conci<sup>1</sup>, Martin Lehmann<sup>2</sup>, Gert Wörheide<sup>1,3,4</sup>

<sup>1</sup> Department of Earth and Environmental Sciences, Paleontology & Geobiology, Ludwig-Maximilians-Universität München, Richard-Wagner-Str. 10, 80333 München, Germany.

<sup>2</sup> Biozentrum der LMU München, Department of Biology I–Botany, Grosshaderner Strasse 2–4, Planegg-Martinsried, Germany.

<sup>3</sup> GeoBio-Center, Ludwig-Maximilians-Universität München, Richard-Wagner-Str. 10, 80333 München, Germany.

<sup>4</sup> SNSB – Bayerische Staatssammlung für Paläontologie und Geologie, Richard-Wagner-Str. 10, 80333 München, Germany.

<sup>*</sup> Corresponding author: sergio.vargas@lmu.de

**Author Contributions:**

**SV** Conceptualization, Methodology, Software, Validation, Formal Analysis, Investigation, Resources, Data Curation, Writing - Original Draft, Visualization, Supervision, Funding Acquisition.
**TZ** Investigation, Formal Analysis.
**NC** Investigation, Formal Analysis, Writing - Review & Editing.
**ML** Investigation. 
**GW** Resources, Writing - Review & Editing, Funding Acquisition.


**In the different folders you can find README files with technical information about data generation.**

**Details about *P. flava* and the aquarium system used for the experiments conducted can be found [here](https://gitlab.lrz.de/palmuc/pinnigorgia_resilience/-/wikis/Resilience-to-climate-change-in-an-octocoral).**

If you use these datasets, scripts, etc. please cite:

**Vargas et al. 2022. Transcriptional Response of the Calcification and Stress Response Toolkits in an Octocoral under Heat and pH Stress. Molecular Ecology 31 (3): 798–810. https://doi.org/10.1111/mec.16266** 

and

**Nicola Conci, Gert Wörheide, Sergio Vargas, New Non-Bilaterian Transcriptomes Provide Novel Insights into the Evolution of Coral Skeletomes, Genome Biology and Evolution, Volume 11, Issue 11, November 2019, Pages 3068–3081, https://doi.org/10.1093/gbe/evz199**

#### Data repositories:
- Raw (MS/MS) proteomic data: https://data.ub.uni-muenchen.de/189/
- Raw nucleotide data: European Nucleotide Archive project PRJEB38757, accession numbers ERS4652944-ERS4652972

#### NOTES

1. This repository (or parts of it) is in active development. Check the releases section to see if there are snap-shots of it.

2. This repository is **public** and the following personal information is visible:
	* After each **commit*, the name and E-mail address as saved in your Git config.
	* The **GitLab username* as saved in your user settings (under Account).

3. In German: Bitte beachten Sie: Bei öffentlichen Projekten sind die folgenden Informationen öffentlich sichtbar (allgemein zugreifbar):
	* Bei jedem Commit der Name und die E-Mail-Adresse, wie sie in der Konfiguration von Git hinterlegt wurden.
	* Bei GitLab der GitLab-Benutzername (Username), wie er bei den Benutzereinstellungen (User Settings) im Abschnitt "Konto" (Account) festgelegt wird. Der Benutzername ist bei persönlichen Projekten im Pfad sichtbar.

#### Note that by commiting to this repository you accept the publication of the information detailed above. More information about public repositories can be find here: https://doku.lrz.de/display/PUBLIC/GitLab


Sergio






<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
